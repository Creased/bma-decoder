/*!
 * Author: Baptiste MOINE <contact@bmoine.fr>
 * Project: Battle.net Mobile Authenticator Decoder
 * Homepage: https://vps.bmoine.fr/bma-decoder/
 * Released: 17/04/2017
 */

$(document).ready(function() {
    args = {
        hashInput:      '#bma-hash',
        form:           '#mainForm',
        output:         '#output',
        serialOutput:   '#bma-serial',
        hextokenOutput: '#bma-hextoken',
        tokenOutput:    '#bma-token'
    };

    $(args.form).submit(function(event) {
        event.preventDefault();
        var hash = document.querySelector(args.hashInput).value;
        var decoded = decodeBMA(hash);

        $(args.serialOutput).val(decoded.serial).prop('type', 'text');
        $(args.hextokenOutput).val(decoded.hextoken).prop('type', 'text');
        $(args.tokenOutput).val(decoded.token).prop('type', 'text');
        $(args.serialOutput + ', label[for="' + args.serialOutput.split('#')[1] + '"]').css('display', 'inline-block');
        $(args.hextokenOutput + ', label[for="' + args.hextokenOutput.split('#')[1] + '"]').css('display', 'inline-block');
        $(args.tokenOutput + ', label[for="' + args.tokenOutput.split('#')[1] + '"]').css('display', 'inline-block');

        $('html, body').animate({
            scrollTop: $(args.output).offset().top
        }, 100);

        return false;
    });

    function b32encode(str, padding) {
        /*!
         * Based on http://www.herongyang.com/Encoding/Base32-Encoding-Algorithm.html
         *
         * Base32 encoding process:
         *     0. Get input string + remove carriage return and new line;
         *     1. Parse bytes from input string;
         *     2. Add padding to divide input bytes to 5-bytes blocks;
         *     3. Create 5-bits groups;
         *     4. Map each group to one printable character based on the Base32 character set;
         *     5. Override characters that aren't based on the input string, but padding;
         *     6. (Facultative) Add padding to create 8-chars blocks;
         *     7. Return the encoded string.
         */
        var opts = {
                'alphabet': 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567',
                'pad': '=',
                'padding': (padding === true) && true || false
            },
            str = str && str.toString().split('\r')[0].split('\n')[0] || '';  // 0
        var left = str.length % 5;  // 5

        // 1
        var out = (function(str) {
            var bits = [];

            for (var i = 0; i < str.length; ++i) {
                bits[i] = ('000000000' + (str[i].charCodeAt(0).toString(2))).substr(-8);
            }

            return bits;
        })(str.split(''));

        // 2
        out = (function(bits) {
            var len = bits.length,
                pad = '00000000';
            var left = 5 - (len % 5);

            if (left < 5) {
                for (var i = 0; i < left; ++i) {
                    bits[len + i] = pad;
                }
            }

            return bits;
        })(out);

        // 3
        out = (function(bits) {
            var len = bits.length,
                parts = [];

            for (var i = 0; i < len; ++i) {
                var j = Math.floor((i / 5));

                parts[j] = parts[j] && parts[j].toString() || '';
                parts[j] += bits[i];
            }

            return parts;
        })(out.join(''));

        // 4
        out = (function(bits, alphabet) {
            var alphabet = alphabet && alphabet.toString() || 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567',
                len = bits.length;

            for (var i = 0; i < len; ++i) {
                var j = parseInt(bits[i], 2);

                bits[i] = alphabet[j];
            }

            return bits;
        })(out, opts.alphabet);

        // 5
        var override = 0;
        switch (left) {
            case 1:
                override = 6;
                break;
            case 2:
                override = 4
                break;
            case 3:
                override = 3;
                break;
            case 4:
                override = 1;
                break;
        }

        for (var i = 0; i < override; ++i) {
            out.pop();
        }

        // 6
        if (opts.padding) {
            for (var i = 0; i < override; ++i) {
                out.push(opts.pad);
            }
        }

        return out.join('');
    }

    function hexarray(str) {
        var hex = [];

        for (var i = 0; i < (str.length / 2); ++i) {
            var j = i * 2;
            hex[i] = [str[j], str[(j+1)]].join('');
        }

        return hex;
    }

    function hextoascii(str) {
        var arr = hexarray(str),
            str = '';

        for (var i = 0; i < arr.length; ++i) {
            str += String.fromCharCode(parseInt(arr[i], 16));
        }

        return str;
    }

    /*!
     * Guide:
     *     0. Install Android SDK and Android Backup Extractor
     *     1. Get the string value of "com.blizzard.bma.AUTH_STORE.HASH" from Battle.net Mobile Authenticator configuration file:
     *        ```bash
     *        adb root
     *        adb shell cat /data/data/com.blizzard.bma/shared_prefs/com.blizzard.bma.AUTH_STORE.xml
     *        ```
     *     2. Decrypt your token using this tool
     *     3. Go to https://vps.bmoine.fr/otp-qrcode/
     *     4. Select Blizzard template and generate a QR-Code using your serial as a username and token as a secret
     *     5. Follow next steps described on OTP QR-Code Generator
     */
    function decodeBMA(hash) {
        var mask = '398e27fc50276a656065b0e525f4c06c04c61075286b8e7aeda59da9813b5dd6c80d2fb38068773fa59ba47c17ca6c6479015c1d5b8b8f6b9a',  // From http://forum.xda-developers.com/showpost.php?p=7303107&postcount=94
            xor = [],
            token = '';
            serial = '';
        hash = hash && hash.toString().split('\r')[0].split('\n')[0] || '';

        hash = hexarray(hash);
        mask = hexarray(mask);

        for (var i = 0; i < hash.length; ++i) {
            xor[i] = (parseInt(hash[i], 16) ^ parseInt(mask[i], 16)).toString(16);
        }

        xor = xor.join('');
        token = hextoascii(xor.substr(0, 80));
        serial = hextoascii(xor.substr(80, xor.length));

        return {
            'hextoken': token,
            'token': b32encode(hextoascii(token)),
            'serial': serial
        }
    }
});
